<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.html">Home</a></li>
          <li>Portfolio Details</li>
        </ol>
        <h2>Portfolio Details</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <?php
        $price = $productsDetails['price'];;
        $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);


        ?>
        <div class="row gy-4">

          <div class="col-lg-8">
            <?php
            $direct = $site_config['direct_message'];
            $nl = "\n";
            $name = $productsDetails['name'];
            $price_product = $fmt->formatCurrency($price, "IDR");
            $sku = $productsDetails['sku'];
            $qty = $productsDetails['quantity'];
            $size = $productsDetails['size'];

            $text = $direct . $nl . "Product Name : " . $name . $nl . "Product Price : " . $price_product . $nl . "Product Size : " .$size;
            $number = $site_config['whatsapp_number'];
            $count = count($productImages);
            if($count > 0){


             ?>
            <div class="portfolio-details-slider swiper-container">

              <div class="swiper-wrapper align-items-center">

                <?php foreach($productImages as $postImg) : ?>
                <div class="swiper-slide">
                  <img src="<?php echo site_url();?>assets/images/products_multiple/<?php echo $postImg['file_name']; ?>" width="300" height="500" alt="">
                </div>
              <?php endforeach ?>


              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
              <div class="swiper-pagination"></div>
            </div>
          <?php } else {

           ?>
           <div class="portfolio-details-slider swiper-container">

             <div class="swiper-wrapper align-items-center">


               <div class="swiper-slide">
                 <img src="<?php echo site_url();?>assets/images/products/<?php echo $productsDetails['image']; ?>" width="300" height="500" alt="">
               </div>



             </div>

               </div>
           <?php } ?>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h3>Product information</h3>
              <ul>
                <li><strong>Price</strong>: <?=$fmt->formatCurrency($price, "IDR");?></li>
                <li><strong>Product SKU</strong>: <?=$productsDetails['sku']?></li>
                <li><strong>Product Name</strong>: <?=$productsDetails['name']?></li>
                <li><strong>Quantity</strong>: <?=$productsDetails['quantity']?></li>
                <li><strong>Size</strong>: <?=$productsDetails['size']?></li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>Description Product</h2>
              <p>
                <?=$productsDetails['description']?>
                </p>
            </div>

              <a href="https://wa.me/<?=$number?>?text=<?=urlencode($text)?>" class="btn btn-danger">Order Now</a>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
