<?php
	class Frontend extends CI_Controller{

		public function view($page = 'home'){
			if (!file_exists(APPPATH.'views/frontend/'.$page.'.php')) {
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['products'] = $this->Administrator_Model->get_products();
			$data['product_categories'] = $this->Administrator_Model->product_categories();
			$site['social'] = $this->Administrator_Model->get_sociallinks();
			$siteconf = $this->Administrator_Model->update_siteconfiguration(1);
			$site['site_config'] = $siteconf;
			$data['site_config'] = $siteconf;
			$this->load->view('templates/header-front', $site);
			$this->load->view('frontend/'.$page, $data);
			$this->load->view('templates/footer-front', $site);
		}
		public function product_detail($id = NULL)
		{
			$data['product_categories'] = $this->Administrator_Model->product_categories();
			$data['productsDetails'] = $this->Administrator_Model->update_products($id);
			$productId = $data['productsDetails']['id'];
			$data['productImages'] = $this->Administrator_Model->product_images($productId);

			if (empty($data['productsDetails'])) {
				show_404();
			}
			$data['title'] = 'Product Detail';
			$siteconf = $this->Administrator_Model->update_siteconfiguration(1);
			$site['site_config'] = $siteconf;
			$data['site_config'] = $siteconf;
			$this->load->view('templates/header-front', $site);
			$this->load->view('frontend/product-detail', $data);
			$this->load->view('templates/footer-front', $site);

		}
	}
